import React from 'react';
import ReactDOM from 'react-dom';
import 'assets/styles/styles.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {SnippetsRender} from 'lib'


ReactDOM.render(<SnippetsRender />, document.getElementById('root'));
registerServiceWorker();



