import { observable, action, computed } from 'mobx';


/* --------------------------------------------------------------  
The Application Store that holds all the app data
-------------------------------------------------------------- */
class Store {
    // Observable array that holds the propSnippets
    @observable propSnippets = [];

    // Observable array that holds the propSnippetValues
    @observable propSnippetValues = [];


    // Observable object that holds all the UI state changes
    @observable uiState = {};

    /* --------------------------------------------------------------  
    Helpers, Getters functions
    -------------------------------------------------------------- */
    getField = (id) => {
        return this.fields.find((field) => {
            return field.id === Number(id);
        });
    }

    getPropSnippet = (id) => {
        return this.propSnippets.find((snippet) => {
            return snippet.id === Number(id);
        });
    }

    getFieldValue = (valueId) => {
        return this.propSnippetValues.find((value) => {
            return value.id === Number(valueId);
        });
    }

    /* --------------------------------------------------------------  
    Actions that modify the Store
    -------------------------------------------------------------- */
    @action ChangeValue = (value, valueId) => {
        let valObj = this.getFieldValue(Number(valueId));
        valObj.value = value;
    }

    @action SetValue = (value, type) => {
        this[type] = value;
        return this[type];
    }
}

export default new Store();

export const sStore = new Store()