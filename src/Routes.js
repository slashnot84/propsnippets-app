import React, { Component } from 'react';
import { Route } from 'react-router-dom';

// Pages
import Home from 'components/pages/Home'
import SnippetDetails from 'components/pages/SnippetDetails'


class Routes extends Component {
    render() {
        return (
            <div>
                <Route exact path="/" component={Home} />
                <Route exact path="/details/:id" component={SnippetDetails} />
            </div>
        );
    }
}

export default Routes;