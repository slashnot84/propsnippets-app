import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import {BrowserRouter as Router} from 'react-router-dom'
import Store from 'Store';

import UIContent from 'components/ui-parts/UIContent'
import UIHeader from 'components/ui-parts/UIHeader'
import UIFooter from 'components/ui-parts/UIFooter'

// Antd UI Elements
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;


class App extends Component {
  render() {
    return (
     <Router>
      <Provider Store={Store}>
        <Layout className="layout">
          <UIHeader></UIHeader>
          <Content className="main">
            <UIContent></UIContent>
          </Content>
          <UIFooter></UIFooter>
        </Layout>
      </Provider>
     </Router>
    );
  }
}

export default App;
