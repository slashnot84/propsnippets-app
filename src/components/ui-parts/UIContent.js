import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { Row, Col } from 'antd';

import Home from 'components/pages/Home'
import Routes from 'Routes'

class UIContent extends Component {
    render() {
        return (
            <div className="main-content">
                <Routes></Routes>
            </div>
        );
    }
}

export default UIContent;