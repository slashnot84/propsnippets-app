import React, { Component } from 'react';
import { Layout } from 'antd';
const { Footer } = Layout;

class UIFooter extends Component {
    render() {
        return (
            <Footer style={{ textAlign: 'center' }}>
              Simple Property Editor
            </Footer>
        );
    }
}

export default UIFooter;