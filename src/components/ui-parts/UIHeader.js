import React, { Component } from 'react';
import { Layout, Menu } from 'antd';

const { Header } = Layout;

class UIHeader extends Component {
    render() {
        return (
            <Header>
                <div className="logo" />
                <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['home']}
                    style={{ lineHeight: '64px', 'display':'flex', 'justifyContent':'flex-end' }}
                >
                    <Menu.Item key="home">Home</Menu.Item>
                </Menu>
            </Header>
        );
    }
}

export default UIHeader;