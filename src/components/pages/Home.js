import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Button, Form } from 'antd';
import { get } from 'axios';

import PropSnippets from 'core/components/PropSnippets'
import FieldLogger from 'core/components/render-tools/FieldLogger'
import SnippetLogger from 'core/components/render-tools/SnippetLogger'

@inject('Store')
@observer
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            propSnippets: null,
            propSnippetValues: null
        }
        this.Store = this.props.Store;
    }

    componentDidMount = () => {
        // Fetch snippets data from server
        get('https://maps-server.herokuapp.com/props')
            .then((response) => {
                this.setState({ propSnippets: response.data });
            })
            .catch((err) => {
                console.log(err);
            });

        // Fetch values data from server
        get('https://maps-server.herokuapp.com/values')
            .then((response) => {
                this.setState({ propSnippetValues: response.data });
            })
            .catch((err) => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="home-page">
                <Row gutter={16}>
                    <Col span={12}>
                        <h2>Home</h2>
                        {/* Prove data from state when the state is changed with the API response */}
                        {this.state.propSnippets && this.state.propSnippetValues ?
                            <PropSnippets snippets={this.state.propSnippets}
                                values={this.state.propSnippetValues}></PropSnippets>
                            :
                            <h3>Loading...</h3>}
                    </Col>
                </Row>

            </div>
        );
    }
}

export default Home;