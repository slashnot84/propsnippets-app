import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Button, Form } from 'antd';
import { Redirect } from 'react-router-dom';

import SnippetLogger from 'core/components/render-tools/SnippetLogger';
import PropSnippet from 'core/components/PropSnippet'


@inject('Store')
@observer
class SnippetDetails extends Component {
    constructor(props) {
        super(props);
        this.Store = props.Store;
    }

    componentWillMount = () => {
        // if(!this.Store.propSnippets){
        //     this.props.history.go('/');
        // }
    }


    render() {
        if (this.Store.propSnippets.length) {
            return (
                <div className="home-page">
                    <Row gutter={16}>
                        <Col span={12}>
                            <PropSnippet snippetId={this.props.match.params.id}></PropSnippet>
                        </Col>
                        <Col span={12}>
                            <SnippetLogger snippetId={this.props.match.params.id}>
                            </SnippetLogger>
                        </Col>
                    </Row>

                </div>
            );
        }
        else {
            return(
                <Redirect to="/"></Redirect>
            )
        }

    }
}

export default SnippetDetails;