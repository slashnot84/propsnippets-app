import adapter from 'mobx-rest-fetch-adapter'
import { apiClient, Collection, Model } from 'mobx-rest'

// We will use the adapter to make the `xhr` calls. Define the base URL here
apiClient(adapter, { apiPath: 'https://jsonplaceholder.typicode.com' })

// Initiate the property Model class and Collection class
class Prop extends Model {

}

class Props extends Collection {
    url() { return `/posts` }
    model() { return Prop }
}
const Properties = new Props();

class Resource {

    /* Queries the properties from the server and returns promise
    ------------------------------------------------------------------- */
    getProperties = () => {
        return Properties.fetch();
    }

    /* Gets a single Property Model from the server and returns promise
    ------------------------------------------------------------------- */
    getProperty = () => {
        return Properties.get(id);
    }
}

export default new Resource();