import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Form, Icon, Input, Button } from 'antd';

const FormItem = Form.Item;
const elmProps = {};


@inject('Store')
@observer
class InputProp extends Component {
    constructor(props) {
        super(props);
        Object.assign(elmProps, this.props);
        delete elmProps.Store;
        delete elmProps.valueId;
        this.Store = this.props.Store;
        this.valueId = this.props.valueId;
    }

    componentWillReceiveProps = (nextProps) => {
        Object.assign(elmProps, nextProps);
        delete elmProps.Store;
        delete elmProps.valueId;
    }

    changeValue = (e) => {
        this.Store.ChangeValue(e.target.value, this.valueId);
    }

    render() {
        let fieldValue = this.Store.getFieldValue(this.valueId).value;
        return (
            <FormItem label={this.props.title}>
                <Input
                    value={fieldValue}
                    onChange={this.changeValue} {...elmProps} />
                <h3>{fieldValue}</h3>
            </FormItem>
        );
    }
}

export default InputProp;