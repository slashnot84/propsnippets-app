import InputProp from 'core/components/prop-elements/InputProp';
import SelectProp from 'core/components/prop-elements/SelectProp';
import SliderProp from 'core/components/prop-elements/SliderProp';
import ColorProp from 'core/components/prop-elements/ColorProp';
import NumberProp from 'core/components/prop-elements/NumberProp';
import './PropStyles.css';

export default {
    InputProp,
    SelectProp,
    SliderProp,
    ColorProp,
    NumberProp
}