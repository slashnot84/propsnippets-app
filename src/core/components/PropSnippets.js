import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Card } from 'antd'
import PropSnippet from 'core/components/PropSnippet';


@inject('Store')
@observer
class PropSnippets extends Component {
    constructor(props) {
        super(props);
        this.Store = props.Store;
    }

    componentDidMount = () => {
        // Move values to local store for reactive rendering
        this.Store.SetValue(this.props.snippets, 'propSnippets');
        this.Store.SetValue(this.props.values, 'propSnippetValues');
    }

    render() {
        return (
            <div className="prop-snippets">
                {this.Store.propSnippets.map((snippet, i) => {
                    return (
                        <Card key={i}>
                            <h3><Link to={"/details/" + snippet.id}>{snippet.title}</Link></h3>
                        </Card>
                    )
                })}
            </div>
        );
    }
}

export default PropSnippets;