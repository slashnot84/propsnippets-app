import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {Card} from 'antd';

@inject('Store')
@observer
class FieldLogger extends Component {
    constructor(props) {
        super(props);
        this.Store = props.Store;
    }
    render() {
        return (
            <div className="field-logger">
                <Card>
                <h2 className="card-title">Field log of {this.Store.getField(this.props.fieldId).title}</h2>
                    <pre>
                        {JSON.stringify(this.Store.getField(this.props.fieldId), null, 2)}
                    </pre>
                </Card>
            </div>
        );
    }
}

export default FieldLogger;