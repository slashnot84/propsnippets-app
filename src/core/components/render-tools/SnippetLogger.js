import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Card } from 'antd';

@inject('Store')
@observer
class SnippetLogger extends Component {
    constructor(props) {
        super(props);
        this.Store = props.Store;
    }

    buildLog = () => {
        let snippet = this.Store.getPropSnippet(this.props.snippetId);

        return snippet.fields.map((field, i) => {
            return (<div key={i}>
                {JSON.stringify({
                    title: field.title,
                    value: this.Store.getFieldValue(field.valueId)
                }, null, 2)}
            </div>)
        })
    }

    render() {
        return (
            <div className="snippet-logger">
                <Card>
                    <h2 className="card-title">Snippet log of {this.Store.getPropSnippet(this.props.snippetId).title}</h2>
                    <pre>
                        {this.buildLog()}
                    </pre>
                </Card>
            </div>
        );
    }
}

export default SnippetLogger;