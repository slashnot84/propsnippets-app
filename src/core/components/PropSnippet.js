import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Form, Button, Icon } from 'antd';
import PropElements from 'core/components/prop-elements'
import { post, put } from 'axios'

const { InputProp, SelectProp, SliderProp, ColorProp, NumberProp } = PropElements;
const FormItem = Form.Item;
const elmProps = {};

/* -------------------------------------------------------------------
    React class to display the property elements
    from the Json data and to link them to Store 
------------------------------------------------------------------- */
@inject('Store')
@observer
class PropSnippet extends Component {
    constructor(props) {
        super(props);
        Object.assign(elmProps, this.props);
        delete elmProps.Store;
        delete elmProps.snippetId;
        delete elmProps.snippet;
        this.id = this.props.snippetId;
        this.Store = props.Store;
    }

    componentWillReceiveProps = (nextProps) => {
        Object.assign(elmProps, nextProps);
        delete elmProps.snippetId;
        delete elmProps.Store;
        delete elmProps.snippet;
    }

    /* Function to render the individual fields from JSON data 
    ------------------------------------------------------------*/
    renderFields = () => {
        /* Access the inbuilt Store and get the snippets */
        const snippet = this.props.snippet ? this.props.snippet : this.Store.getPropSnippet(this.id);

        const elements = snippet.fields.map((field, id) => {
            switch (field.type.toLowerCase()) {
                case 'input':
                    return <InputProp key={id} {...field}></InputProp>
                    break;

                case 'number':
                    return <NumberProp key={id} {...field}></NumberProp>
                    break;

                case 'select':
                    return <SelectProp key={id} {...field}></SelectProp>
                    break;

                case 'color':
                    return <ColorProp key={id} {...field}></ColorProp>
                    break;

                case 'slider':
                    return <SliderProp key={id} {...field}></SliderProp>
                    break;
                default:
                    break;
            }
        })

        return (
            <div>
                {elements}
            </div>
        )
    }

    render() {
        console.log(this.Store)
        return (
            <div>
                <h2>{this.Store.getPropSnippet(this.props.snippetId).title}</h2>
                <Form {...elmProps}>
                    {this.renderFields(this.props.fields)}
                </Form>
            </div>
        );
    }
}

export default PropSnippet;