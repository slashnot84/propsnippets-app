This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Online Demo
Deployed [Demo on Heroku](https://prop-snippet-app.herokuapp.com/)

## Dev Usage
```
npm install json-server -g
This installs the fake json based REST server globally.

git clone https://slashnot84@bitbucket.org/slashnot84/propsnippets-app.git 
cd propsnippets-app
npm install

npm start
This starts the development server on port 3000. The app will run on [http://localhost:3000](http://localhost:3000)
```

## Usage in other projects
```
npm install https://slashnot84@bitbucket.org/slashnot84/propsnippets-app.git --save
```

### Usage

```
import {PropSnippets,PropSnippet,InputProp,
NumberProp,SelectProp,ColorProp,SliderProp,SnippetLogger,
SnippetsRender} from 'propsnippets-app';
```
### Using the PropSnippets in react app
This component renders the HTML elements from a JSON metadata. Usually in real-life apps, this JSON meta comes from
a REST API backend. See the source for how to use it.
```
<PropSnippets snippets={this.state.propSnippets}
values={this.state.propSnippetValues}></PropSnippets>

```
### Sample metadata
```
{
    "props": [
        {
            "id": 1,
            "title": "Sample Properties",
            "fields": [
                {
                    "title": "Sample Properties",
                    "type": "input",
                    "defaultValue": "Sample Properties",
                    "placeholder": "Input Placeholder",
                    "valueId": 1
                },
                {
                    "title": "Sample Number",
                    "type": "number",
                    "defaultValue": 1,
                    "placeholder": "Number Placeholder",
                    "valueId": 2
                },
                {
                    "title": "Sample Slider",
                    "type": "slider",
                    "defaultValue": 10,
                    "min": 1,
                    "max": 20,
                    "valueId": 3
                },
                {
                    "title": "Sample Color",
                    "type": "color",
                    "valueId": 4
                },
                {
                    "title": "Sample Select",
                    "type": "select",
                    "placeholder": "Select an option",
                    "options": [
                        {
                            "key": "0",
                            "value": "Select value 1"
                        },
                        {
                            "key": "1",
                            "value": "Select value 2"
                        }
                    ],
                    "valueId": 5
                }
            ]
        },
        {
            "id": 2,
            "title": "CSS Properties",
            "fields": [
                {
                    "title": "Width",
                    "type": "number",
                    "defaultValue": 0,
                    "placeholder": "in px",
                    "valueId": 6
                },
                {
                    "title": "Height",
                    "type": "number",
                    "defaultValue": 0,
                    "placeholder": "in px",
                    "valueId": 7
                },
                {
                    "title": "Background",
                    "type": "color",
                    "valueId": 8
                },
                {
                    "title": "Border-Radius",
                    "type": "slider",
                    "min": 0,
                    "max": 20,
                    "valueId": 9
                },
                {
                    "title": "Display",
                    "type": "select",
                    "placeholder": "Select an option",
                    "options": [
                        {
                            "key": "0",
                            "value": "Block"
                        },
                        {
                            "key": "1",
                            "value": "Inline-Block"
                        },
                        {
                            "key": "2",
                            "value": "Flex"
                        }
                    ],
                    "valueId": 10
                }
            ]
        }
    ],
    "values": [
        {
            "id": 1,
            "value": "Input Value"
        },
        {
            "id": 2,
            "value": 20
        },
        {
            "id": 3,
            "value": 16
        },
        {
            "id": 4,
            "value": "#FF00F0"
        },
        {
            "id": 5,
            "value": "Some select val"
        },
        {
            "id": 6,
            "value": 20
        },
        {
            "id": 7,
            "value": 20
        },
        {
            "id": 8,
            "value": "#F0F0FF"
        },
        {
            "id": 9,
            "value": 10
        },
        {
            "id": 10,
            "value": "Flex"
        }
    ]
}
```
### Using the InputProp and other element props in react app
```
<InputProp {FieldInfo object}>
```

### Sample Field object
```
{
                    "title": "Width",
                    "type": "number",
                    "defaultValue": 0,
                    "placeholder": "in px",
                    "valueId": 6
                }
```
where valueId is the id of the value in the value object.

